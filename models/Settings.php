<?php

namespace Devinci\TribeHr\Models;

use October\Rain\Database\Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'devinci_tribehr_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
