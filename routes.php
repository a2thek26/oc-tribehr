<?php

use Illuminate\Support\Facades\Input;

Route::any(config('devinci.tribehr::config.webhook_url', '/pto-request'), function(){
    $webhook = Input::all();
    Event::fire('webhook.pto-request', [$webhook]);
});
