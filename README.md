# TribeHR

## PTO Request
In TribeHR, we are afforded the ability to add "webhooks" to predefined events that occur within the system. This means we can associate a url to an event - every time the event happens it will send information to the provided url.  Part of this plugin's job is to be the endpoint for the webhook. Our system "listens" for when someone requests time off. When that info is broadcast, we grab the event's object_id and utilize Tribe's API in order to get the exact information about the event. From that we can determine who is requesting time of and what the start and end dates are for when they are requesting. Currently, we're just sending an email to a list of folks with the information in it.
