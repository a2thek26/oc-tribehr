<?php
namespace Devinci\TribeHr\Services;

use App;
use Devinci\TribeHr\Models\Settings;
use Mail;

class TribeService
{

    public function handle($data)
    {
        if(isset($data['object_id'])) {
            $recipients = explode("\r\n",  Settings::get('recipients'));
            $tribe = App::make('Tribe');
            $data  = $tribe->request('leave_requests/'.$data['object_id'].'.json');
            $response = (array) json_decode($data);

            Mail::sendTo($recipients, 'devinci.tribehr::mail.pto-request',  $response);
        }

    }

}
