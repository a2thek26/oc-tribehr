<?php
namespace Devinci\TribeHr;

use Backend;
use Devinci\TribeHr\Models\Settings;
use Event;
use Hussainweb\TribeHr\Client;
use System\Classes\PluginBase;

/**
 * TribeHr Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'TribeHR',
            'description' => 'Interact with TribeHR webhooks',
            'author'      => 'Devinci',
            'icon'        => 'icon-plug'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'PTO Request',
                'description' => 'Notify management of pending PTO requests',
                'category'    => 'TribeHR',
                'icon'        => 'icon-envelope',
                'class'       => 'Devinci\TribeHr\Models\Settings',
                'order'       => 500,
                'keywords'    => 'tribe, hr, pto, request, notifications',
                'permissions' => ['devinci.tribehr.access_settings']
            ]
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'devinci.tribehr::mail.pto-request' => 'Notification sent to recipients that are tracking employee PTO requests.'
        ];
    }

    /**
     * Register class bindings
     */
    public function register()
    {
        $this->app->bind('GuzzleHttp\ClientInterface', 'GuzzleHttp\Client');

        $this->app->bind('Tribe', function($app) {
            $client = $app->make('GuzzleHttp\ClientInterface');

            return new Client($client, Settings::get('subdomain'), Settings::get('username'), Settings::get('api_key'));
        });
    }

    /**
     * Extend other classes and set event listeners
     */
    public function boot()
    {
        Event::listen('webhook.pto-request', 'Devinci\TribeHr\Services\TribeService@handle');
    }

}
